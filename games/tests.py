from django.test import TestCase
from django.core.urlresolvers import reverse
from django.utils.http import urlencode
from rest_framework import status
from rest_framework.test import APITestCase
from .models import GameCategory


class GameCategoryTests(APITestCase):
    def create_game_category(self, name):
        url = reverse('gamecategory-list')
        data = {'name': name}
        response = self.client.post(url, data, format='json')
        return response

    def test_create_and_retrieve_game_category(self):
        """
        Ensure we can create a new GameCategory and then retrieve it
        """
        new_game_category = 'New Game Category'
        response_post = self.create_game_category(new_game_category)
        self.assertEqual(response_post.status_code, status.HTTP_201_CREATED)
        self.assertEqual(GameCategory.objects.get().name, new_game_category)

        response_get = self.client.get(reverse('gamecategory-list'))
        self.assertEqual(response_get.status_code, status.HTTP_200_OK)
        self.assertEqual(response_get.data['count'], 1)
        self.assertEqual(GameCategory.objects.get().name, response_get.data['results'][0]['name'])
        print('PK {0}'.format(GameCategory.objects.get().pk))

    def test_create_duplicated_game_category(self):
        new_game_category = 'New Game Category'
        response1 = self.create_game_category(new_game_category)
        self.assertEqual(response1.status_code, status.HTTP_201_CREATED)
        response2 = self.create_game_category(new_game_category)
        self.assertEqual(response2.status_code, status.HTTP_400_BAD_REQUEST)

    def test_retrieve_game_categories_list(self):
        new_game_category = 'New Game Category'
        self.create_game_category(new_game_category)
        response = self.client.get(reverse('gamecategory-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 1)
        self.assertEqual(response.data['results'][0]['name'], new_game_category)

    def test_update_game_category(self):
        new_game_category = 'Initial Name'
        response = self.create_game_category(new_game_category)
        url = reverse('gamecategory-detail', None, {response.data['pk']})
        updated_game_category_name = 'Updated Game Category Name'
        data = {'name': updated_game_category_name}
        patch_response = self.client.patch(url, data, format='json')
        self.assertEqual(patch_response.status_code, status.HTTP_200_OK)
        self.assertEqual(patch_response.data['name'], updated_game_category_name)

    def test_filter_game_category(self):
        game_category_name1 = 'First Game Category Name'
        self.create_game_category(game_category_name1)
        game_category_name2 = 'Second Game Category Name'
        self.create_game_category(game_category_name2)
        filter_by_name = {'name': game_category_name1}
        url = '{0}?{1}'.format(
            reverse('gamecategory-list'),
            urlencode(filter_by_name)
        )
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 1)
        self.assertEqual(response.data['results'][0]['name'], game_category_name1)
